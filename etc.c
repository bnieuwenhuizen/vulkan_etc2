#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vulkan.h>

#include "sha1/sha1.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define DIV_ROUND_UP(a, b) (((a) + (b)-1) / (b))
#define MAX2(a, b) ((a) < (b) ? (b) : (a))
#define MIN2(a, b) ((a) < (b) ? (a) : (b))

const bool dump_data = false;

static VkInstance g_instance;
static VkPhysicalDevice g_pdevice;
static VkPhysicalDeviceMemoryProperties g_memory_props;
static VkDevice g_device;
static VkQueue g_queue;
static VkCommandPool g_cmdpool;

#define CHECK(v)                                                                                   \
  do {                                                                                             \
    VkResult result = (v);                                                                         \
    if (result != VK_SUCCESS && result != VK_INCOMPLETE) {                                         \
      fprintf(stderr, "Vulkan error %d in %s\n", result, #v);                                      \
      exit(1);                                                                                     \
    }                                                                                              \
  } while (0)
void start() {
  CHECK(vkCreateInstance(&(VkInstanceCreateInfo){.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO},
                         NULL, &g_instance));

  uint32_t devcount = 1;
  CHECK(vkEnumeratePhysicalDevices(g_instance, &devcount, &g_pdevice));
  if (!devcount) {
    fprintf(stderr, "Did not find any vulkan devices\n");
    exit(1);
  }

  vkGetPhysicalDeviceMemoryProperties(g_pdevice, &g_memory_props);

  CHECK(vkCreateDevice(
      g_pdevice,
      &(const VkDeviceCreateInfo){
          .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
          .queueCreateInfoCount = 1,
          .pQueueCreateInfos =
              &(VkDeviceQueueCreateInfo){.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                                         .queueFamilyIndex = 0,
                                         .queueCount = 1,
                                         .pQueuePriorities = (const float[]){0.5f}}},
      NULL, &g_device));

  vkGetDeviceQueue(g_device, 0, 0, &g_queue);

  CHECK(vkCreateCommandPool(
      g_device,
      &(VkCommandPoolCreateInfo){.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                                 .queueFamilyIndex = 0},
      NULL, &g_cmdpool));
}

VkImage create_image(VkFormat format, VkExtent3D extent, unsigned layers, unsigned levels) {
  VkImage img;

  CHECK(
      vkCreateImage(g_device,
                    &(VkImageCreateInfo){
                        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                        .imageType = VK_IMAGE_TYPE_2D,
                        .format = format,
                        .extent = extent,
                        .mipLevels = levels,
                        .arrayLayers = layers,
                        .samples = VK_SAMPLE_COUNT_1_BIT,
                        .tiling = VK_IMAGE_TILING_OPTIMAL,
                        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT |
                                 VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    },
                    NULL, &img));

  unsigned mem_idx = 0;
  for (unsigned i = 0; i < g_memory_props.memoryTypeCount; ++i) {
    if ((g_memory_props.memoryTypes[i].propertyFlags &
         (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT)) ==
        (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT)) {
      mem_idx = i;
      break;
    }
  }
  VkMemoryRequirements req;
  vkGetImageMemoryRequirements(g_device, img, &req);
  VkDeviceMemory mem;
  CHECK(vkAllocateMemory(g_device,
                         &(VkMemoryAllocateInfo){.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                                                 .allocationSize = req.size,
                                                 .memoryTypeIndex = mem_idx},
                         NULL, &mem));

  CHECK(vkBindImageMemory(g_device, img, mem, 0));
  return img;
}

VkBuffer create_buffer(size_t size, void **ptr) {
  VkBuffer buf;

  CHECK(vkCreateBuffer(
      g_device,
      &(VkBufferCreateInfo){
          .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
          .size = size,
          .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
          .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      },
      NULL, &buf));

  unsigned mem_idx = 0;
  for (unsigned i = 0; i < g_memory_props.memoryTypeCount; ++i) {
    if ((g_memory_props.memoryTypes[i].propertyFlags &
         (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT)) ==
        (VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT)) {
      mem_idx = i;
      break;
    }
  }
  VkMemoryRequirements req;
  vkGetBufferMemoryRequirements(g_device, buf, &req);
  VkDeviceMemory mem;
  CHECK(vkAllocateMemory(g_device,
                         &(VkMemoryAllocateInfo){.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                                                 .allocationSize = req.size,
                                                 .memoryTypeIndex = mem_idx},
                         NULL, &mem));

  CHECK(vkBindBufferMemory(g_device, buf, mem, 0));
  if (ptr) {
    CHECK(vkMapMemory(g_device, mem, 0, req.size, 0, ptr));
  }
  return buf;
}

VkCommandBuffer create_cmd_buffer() {
  VkCommandBuffer buf;
  CHECK(vkAllocateCommandBuffers(
      g_device,
      &(VkCommandBufferAllocateInfo){.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                                     .commandPool = g_cmdpool,
                                     .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                                     .commandBufferCount = 1},
      &buf));
  return buf;
}

const uint64_t rgb888_data[] = {
   /* ETC1 cases */
   0xAAAA0FAA4C4589AFULL, /* diff = 0, flip = 0 */
   0xAAAA0FAA6D4589AFULL, /* diff = 0, flip = 1 */
   0xAAAA0FAA32849342ULL, /* diff = 1, flip = 0 */
   0x12340FAA4F849342ULL, /* diff = 1, flip = 1 */
   /* New ETC2 cases */
   0x33AA0FAA328493F9ULL, /* T mode */
   0xAAAA0FAA3284F942ULL, /* H mode */
   0x76FE0FAA32F99342ULL, /* planar mode */
};

const uint64_t rgba8881_data[] = {
   0xAAAA0FAA4C4589AFULL,
   0xAAAA0FAA4E4589AFULL,
   0xAAAA0FAA6C4589AFULL,
   0xAAAA0FAA30849342ULL,
   0x12340FAA4D849342ULL,
   0x33AA0FAA328493F9ULL,
   0xAAAA0FAA3284F942ULL,
   0x76FE0FAA32F99342ULL,
};

const uint64_t rgba8888_data[] = {
   0xAAAA0FAA4C4589AFULL, /* diff = 0, flip = 0 */
   0xFAC688681F2EE380ULL,
   0xAAAA0FAA6D4589AFULL, /* diff = 0, flip = 1 */
   0xFAC688681F2EE331ULL,
   0xAAAA0FAA32849342ULL, /* diff = 1, flip = 0 */
   0xFAC688681F2EE3E7ULL,
   0x12340FAA4F849342ULL, /* diff = 1, flip = 1 */
   0xFAC688681F2E9F30ULL,
   0x33AA0FAA328493F9ULL, /* T mode */
   0xFAC688681F2EE380ULL,
   0xAAAA0FAA3284F942ULL, /* H mode */
   0xFAC688681F2EE331ULL,
   0x76FE0FAA32F99342ULL, /* planar mode */
   0xFAC688681F2EE3E7ULL,
};

const uint64_t eac_data[] = {
   0xFAC688681F2EE380ULL,
   0xFAC688681F2EE331ULL,
   0xFAC688681F2EE3E7ULL,
   0xFAC688681F2E9F30ULL,
};

struct format_info {
  VkFormat compressed_format;
  unsigned block_width;
  unsigned block_height;
  unsigned block_size;

  const void *src_data;
  unsigned src_elems;
} formats[] = {
    {VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK, 4, 4, 8, rgb888_data, ARRAY_SIZE(rgb888_data)},
    {VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK, 4, 4, 8, rgb888_data, ARRAY_SIZE(rgb888_data)},
    {VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK, 4, 4, 8, rgba8881_data, ARRAY_SIZE(rgba8881_data)},
    {VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK, 4, 4, 8, rgba8881_data, ARRAY_SIZE(rgba8881_data)},
    {VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK, 4, 4, 16, rgba8888_data, ARRAY_SIZE(rgba8888_data) / 2},
    {VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK, 4, 4, 16, rgba8888_data, ARRAY_SIZE(rgba8888_data) / 2},
    {VK_FORMAT_EAC_R11_UNORM_BLOCK, 4, 4, 8, eac_data, ARRAY_SIZE(eac_data)},
    {VK_FORMAT_EAC_R11_SNORM_BLOCK, 4, 4, 8, eac_data, ARRAY_SIZE(eac_data)},
    {VK_FORMAT_EAC_R11G11_UNORM_BLOCK, 4, 4, 16, eac_data, ARRAY_SIZE(eac_data) / 2},
    {VK_FORMAT_EAC_R11G11_SNORM_BLOCK, 4, 4, 16, eac_data, ARRAY_SIZE(eac_data) / 2},
};

void testcase(unsigned width, unsigned height, unsigned layers, unsigned levels) {
  assert(levels >= 1);
  assert((width >= (1u << (levels - 1))) || (height >= (1u << (levels - 1))));
  for (unsigned f = 0; f < ARRAY_SIZE(formats); ++f) {
    unsigned level_slices[16] = {0};
    unsigned level_strides[16] = {0};
    unsigned total_elems = 0;
    unsigned dst_level_slices[16] = {0};
    unsigned dst_level_strides[16] = {0};
    unsigned dst_total_elems = 0;
    for (unsigned l = 0; l < levels; ++l) {
      unsigned w = DIV_ROUND_UP(MAX2(1, width >> l), formats[f].block_width);
      unsigned h = DIV_ROUND_UP(MAX2(1, height >> l), formats[f].block_height);

      level_strides[l] = w;
      level_slices[l] = w * h;
      total_elems += w * h * layers;

      dst_level_strides[l] = MAX2(1, width >> l);
      dst_level_slices[l] = dst_level_strides[l] * MAX2(1, height >> l);
      dst_total_elems = layers * dst_level_slices[l];
    }
    void *buf_ptr = NULL;
    size_t buf_size = total_elems * formats[f].block_size;
    VkBuffer buf = create_buffer(buf_size, &buf_ptr);
    memset(buf_ptr, 0, buf_size);

    if (formats[f].src_elems) {
      for (size_t offset = 0; offset < buf_size;) {
        size_t sz = MIN2(buf_size - offset, formats[f].src_elems * formats[f].block_size);
        memcpy(buf_ptr + offset, formats[f].src_data, sz);
        offset += sz;
      }
    }
    void *dst_ptr = NULL;
    size_t dst_buf_size = dst_total_elems * 8;
    VkBuffer dst_buf = create_buffer(dst_buf_size, &dst_ptr);
    memset(dst_ptr, 0xE0, dst_buf_size);

    VkImage img =
        create_image(formats[f].compressed_format, (VkExtent3D){width, height, 1}, layers, levels);
    VkImage dst_img =
        create_image(VK_FORMAT_R16G16B16A16_SNORM, (VkExtent3D){width, height, 1}, layers, levels);

    VkCommandBuffer cmd = create_cmd_buffer();

    CHECK(vkBeginCommandBuffer(cmd, &(VkCommandBufferBeginInfo){
                                        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                                    }));

    vkCmdPipelineBarrier(
        cmd, VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 1,
        (VkBufferMemoryBarrier[]){{.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                                   .srcAccessMask = VK_ACCESS_HOST_WRITE_BIT,
                                   .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
                                   .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                   .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                   .buffer = buf,
                                   .offset = 0,
                                   .size = buf_size}},
        2,
        (VkImageMemoryBarrier[]){
            {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
             .srcAccessMask = 0,
             .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
             .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
             .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
             .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .image = img,
             .subresourceRange = (VkImageSubresourceRange){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                                           .baseMipLevel = 0,
                                                           .levelCount = levels,
                                                           .baseArrayLayer = 0,
                                                           .layerCount = layers}},
            {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
             .srcAccessMask = 0,
             .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
             .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
             .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
             .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .image = dst_img,
             .subresourceRange = (VkImageSubresourceRange){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                                           .baseMipLevel = 0,
                                                           .levelCount = levels,
                                                           .baseArrayLayer = 0,
                                                           .layerCount = layers}}});

    unsigned offset = 0;
    for (unsigned le = 0; le < levels; ++le) {
      vkCmdCopyBufferToImage(
          cmd, buf, img, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
          &(VkBufferImageCopy){
              .bufferOffset = offset,
              .imageSubresource =
                  (VkImageSubresourceLayers){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                             .mipLevel = le,
                                             .baseArrayLayer = 0,
                                             .layerCount = layers},
              .imageExtent = (VkExtent3D){MAX2(1, width >> le), MAX2(1, height >> le), 1},

          });
      offset += level_slices[le] * layers * formats[f].block_size;
    }

    vkCmdPipelineBarrier(
        cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
        (VkImageMemoryBarrier[]){
            {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
             .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
             .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
             .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
             .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
             .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .image = img,
             .subresourceRange = (VkImageSubresourceRange){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                                           .baseMipLevel = 0,
                                                           .levelCount = levels,
                                                           .baseArrayLayer = 0,
                                                           .layerCount = layers}}});
    for (unsigned le = 0; le < levels; ++le) {
      vkCmdBlitImage(
          cmd, img, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dst_img,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
          &(VkImageBlit){
              .srcSubresource =
                  (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, le, 0, layers},
              .srcOffsets = {(VkOffset3D){0, 0, 0},
                             (VkOffset3D){MAX2(1, width >> le), MAX2(1, height >> le), 1}},
              .dstSubresource =
                  (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, le, 0, layers},
              .dstOffsets = {(VkOffset3D){0, 0, 0},
                             (VkOffset3D){MAX2(1, width >> le), MAX2(1, height >> le), 1}},
          },
          VK_FILTER_NEAREST);
    }

    vkCmdPipelineBarrier(
        cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
        (VkImageMemoryBarrier[]){
            {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
             .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
             .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
             .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
             .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
             .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
             .image = dst_img,
             .subresourceRange = (VkImageSubresourceRange){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                                           .baseMipLevel = 0,
                                                           .levelCount = levels,
                                                           .baseArrayLayer = 0,
                                                           .layerCount = layers}}});
    offset = 0;
    for (unsigned le = 0; le < levels; ++le) {
      vkCmdCopyImageToBuffer(
          cmd, dst_img, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dst_buf, 1,
          &(VkBufferImageCopy){
              .bufferOffset = offset,
              .imageSubresource =
                  (VkImageSubresourceLayers){.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                             .mipLevel = le,
                                             .baseArrayLayer = 0,
                                             .layerCount = layers},
              .imageExtent = (VkExtent3D){MAX2(1, width >> le), MAX2(1, height >> le), 1},

          });
      offset += dst_level_slices[le] * layers * 8;
    }

    vkCmdPipelineBarrier(
        cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_HOST_BIT, 0, 0, NULL, 2,
        (VkBufferMemoryBarrier[]){{.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                                   .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                                   .dstAccessMask = VK_ACCESS_HOST_READ_BIT,
                                   .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                   .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                   .buffer = dst_buf,
                                   .offset = 0,
                                   .size = dst_buf_size}},
        0, NULL);
    CHECK(vkEndCommandBuffer(cmd));

    CHECK(vkQueueSubmit(g_queue, 1,
                        &(VkSubmitInfo){.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                        .commandBufferCount = 1,
                                        .pCommandBuffers = &cmd},
                        VK_NULL_HANDLE));
    CHECK(vkQueueWaitIdle(g_queue));

    uint8_t hash[SHA1_DIGEST_LENGTH];
    
    SHA1_CTX sha1_ctx;
    SHA1Init(&sha1_ctx);
    SHA1Update(&sha1_ctx, dst_ptr, dst_buf_size);
    SHA1Final(hash, &sha1_ctx);
    fprintf(stdout, "finished format %d ", formats[f].compressed_format);
    for (unsigned i = 0; i < ARRAY_SIZE(hash); ++i) {
       fprintf(stdout, "%.2x", (unsigned)hash[i]);
    }
    fprintf(stdout, "\n");
    if (dump_data) {
      const uint64_t *pixels = dst_ptr;
      for (unsigned y = 0; y < height; ++y) {
         for (unsigned x = 0; x < width; ++x) {
         fprintf(stdout, "%lx ", pixels[x + y * width]);
         }
         fprintf(stdout, "\n");
      }
    }
  }
}

int main() {
  start();
  testcase(1920, 1080, 2, 5);
  return 0;
}
